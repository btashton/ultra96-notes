# Building

Make sure we have modern gcc
```
# 1. Install a package with repository for your system:
# On CentOS, install package centos-release-scl available in CentOS repository:
$ sudo yum install centos-release-scl

# On RHEL, enable RHSCL repository for you system:
$ sudo yum-config-manager --enable rhel-server-rhscl-7-rpms

# 2. Install the collection:
$ sudo yum install devtoolset-4

# 3. Start using software collections:
$ scl enable devtoolset-4 bash
```


There is support for this board in the upstream u-boot, but there are a lot of
patches for the zyncmp platform that have not made it in yet, and without
some of those the board does not seem to boot reliably.

The BSP for the Ultra96 board just uses `xilinx_zynqmp_zcu100_revC_defconfig`.

IMPORTANT:  The psu_init files included in u-boot may need to be updated
based on the board design files.  This is brought up in this commit
`arm/arm64: zynq/zynqmp: pass the PS init file as a kconfig variable`

Make sure the environment is set up correctly
```bash
export CROSS_COMPILE=aarch64-linux-gnu-
```

Now lets build:
```bash
xilinx_zynqmp_zcu100_revC_defconfig
make
```

Should end with something like this
```
  MKIMAGE spl/boot.bin
  MKIMAGE u-boot.img
  LD      u-boot.elf
  CHK     include/config.h
  CFG     u-boot.cfg
  CFGCHK  u-boot.cfg
```

## Extra Configuration Notes

* Enable USB Ethernet  https://github.com/u-boot/u-boot/blob/master/doc/README.usb#L67