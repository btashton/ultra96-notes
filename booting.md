# Booting
The boot process is defined in detail here:
>UG1086 Chapter 11 Boot and Configuration https://www.xilinx.com/support/documentation/user_guides/ug1085-zynq-ultrascale-trm.pdf

For now we are just going to look at the process for booting off of an SD card with no extra encryption or authentication. The main entry point for us is then the BOOT.bin file.

## Investigating BOOT.bin
The possible functionality supported by the boot file is quite extensive due to all the boot and security modes that are supported by the hardware. Writing a complete tool to analyse and generate outside of very specific cases, so for now we can just use the binary tools provided by Xilinx.

> Looks like someone actually made a bunch of progress on this for generating the file here https://github.com/antmicro/zynq-mkbootimage

I did find a utility that does not seem to be referenced by any of the Xilinx documentation called `bootgen_utility` and interesting name because it only analyses the boot file while `bootgen` is a utility that only generates the file.

Anyway... taking the prebuilt file from the BSP

```bash
[bashton@xilinx images]$ bootgen_utility -arch zynqmp -bin BOOT.BIN


****** Xilinx Bootgen Utility v2018.1
  **** Build date : Jun 14 2018-20:18:34
    ** Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.

File: info.txt generated successfully
[bashton@xilinx images]$ cat info.txt 
Xilinx Bootgen Utility
Version: 2018.1





~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
:::::::::::::::::::::::::::::::::::::::::   B O O T    H E A D E R   :::::::::::::::::::::::::::::::::::::::::
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
<Flash Address>     <Offset>            <Description>                     <Interpretation>

[0x00000000]         (0x00)        ARM Vector Table                     -   0x14000000 
[0x00000004]         (0x04)        ARM Vector Table                     -   0x14000000 
[0x00000008]         (0x08)        ARM Vector Table                     -   0x14000000 
[0x0000000c]         (0x0c)        ARM Vector Table                     -   0x14000000 
[0x00000010]         (0x10)        ARM Vector Table                     -   0x14000000 
[0x00000014]         (0x14)        ARM Vector Table                     -   0x14000000 
[0x00000018]         (0x18)        ARM Vector Table                     -   0x14000000 
[0x0000001c]         (0x1c)        ARM Vector Table                     -   0x14000000 
[0x00000020]         (0x20)        Width Detection Word                 -   0xaa995566 
[0x00000024]         (0x24)        Header Signature                     -   0x584c4e58 
[0x00000028]         (0x28)        Encryption Key Source                -   0x00 
                                                                          # Key Source : [Un-encypted]
[0x0000002c]         (0x2c)        FSBL Execution Address               -   0xfffc0000 
[0x00000030]         (0x30)        Source Offset                        -   0x2800 
[0x00000034]         (0x34)        PMU Fw Image Byte Length             -   0x1fae0 
[0x00000038]         (0x38)        Total PMU Image Byte Length          -   0x1fae0 
[0x0000003c]         (0x3c)        FSBL Image Byte Length               -   0x172d8 
[0x00000040]         (0x40)        Total FSBL Image Byte Length         -   0x172d8 
[0x00000044]         (0x44)        FSBL Image Attributes                -   0x800 
                                                                          # Reserved   : 
                                                                          # RSA Enabled: [No RSA]
                                                                          # Hashing    : [SHA-3]
                                                                          # Core       : [A53 Single 64-bit]
                                                                          # Integrity  : [No Integrity Check]
                                                                          # Reserved   : 
                                                                          # Auth Only  : [No]
                                                                          # Opt Key    : [Root key used]
[0x00000048]         (0x48)        Header Checksum                      -   0xfd1750d1 
[0x0000004c]         (0x4c)        BH Key                               -   0x00 
[0x00000050]         (0x50)        BH Key                               -   0x00 
[0x00000054]         (0x54)        BH Key                               -   0x00 
[0x00000058]         (0x58)        BH Key                               -   0x00 
[0x0000005c]         (0x5c)        BH Key                               -   0x00 
[0x00000060]         (0x60)        BH Key                               -   0x00 
[0x00000064]         (0x64)        BH Key                               -   0x00 
[0x00000068]         (0x68)        BH Key                               -   0x00 
[0x00000098]         (0x6C)        Reserved                             -   0x1000020 
[0x00000098]         (0x98)        Image Header Table Offset            -   0x8c0 
[0x0000009c]         (0x9C)        Partition Header Table Offset        -   0x1100 
[0x000000a0]         (0xA0)        Secure Header IV                     -   0x00 
[0x000000a4]         (0xA4)        Secure Header IV                     -   0x00 
[0x000000a8]         (0xA8)        Secure Header IV                     -   0x00 
[0x000000ac]         (0xAC)        BH Key IV                            -   0x00 
[0x000000b0]         (0xB0)        BH Key IV                            -   0x00 
[0x000000b4]         (0xB4)        BH Key IV                            -   0x00 





~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
:::::::::::::::::::::::   R E G I S T E R    I N I T I A L I Z A T I O N    T A B L E   ::::::::::::::::::::::
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

[0x000000b8]
		Refer file "_register_init_table.txt"
[0x000008b8]




~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
::::::::::::::::::::::::::::::::::   I M A G E    H E A D E R    T A B L E   :::::::::::::::::::::::::::::::::
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
<Flash Address>     <Offset>            <Description>                     <Interpretation>

[0x000008c0]         (0x00)        Version                              -   0x1020000 
[0x000008c4]         (0x04)        Count of Image Headers               -   0x04 
[0x000008c8]         (0x08)        Offset to 1st Partition Header (Word)-   0x440 
[0x000008cc]         (0x0c)        Offset to 1st Image Header (Word)    -   0x240 
[0x000008d0]         (0x10)        Offset to Header Auth. Cert. (Word)  -   0x00 
[0x000008d4]         (0x14)        Boot Device Type                     -   0x00 
                                                                          # Boot Device : [None]
[0x000008fc]         (0x3c)        Image Header Table Checksum          -   0xfefdf97b 





~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
::::::::::::::::::::::::::::::::::::::::   I M A G E    H E A D E R  :::::::::::::::::::::::::::::::::::::::::
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~    Image 1    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
<Flash Address>     <Offset>            <Description>                     <Interpretation>

[0x00000900]         (0x00)        Next Image Header Pointer (Word)     -   0x250 
[0x00000904]         (0x04)        Next 1st Partition Header (Word)     -   0x440 
[0x00000908]         (0x08)        Partition Count (Wrong Info)         -   0x00 
[0x0000090c]         (0x0C)        Image Length (Wrong Info)            -   0x01 
[0x00000910]         (0x10)        Image Name                           -   zynqmp_fsbl.elf




~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
::::::::::::::::::::::::::::::::::::::::   I M A G E    H E A D E R  :::::::::::::::::::::::::::::::::::::::::
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~    Image 2    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
<Flash Address>     <Offset>            <Description>                     <Interpretation>

[0x00000940]         (0x00)        Next Image Header Pointer (Word)     -   0x260 
[0x00000944]         (0x04)        Next 1st Partition Header (Word)     -   0x450 
[0x00000948]         (0x08)        Partition Count (Wrong Info)         -   0x00 
[0x0000094c]         (0x0C)        Image Length (Wrong Info)            -   0x01 
[0x00000954]         (0x10)        Image Name                           -   download.bit




~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
::::::::::::::::::::::::::::::::::::::::   I M A G E    H E A D E R  :::::::::::::::::::::::::::::::::::::::::
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~    Image 3    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
<Flash Address>     <Offset>            <Description>                     <Interpretation>

[0x00000980]         (0x00)        Next Image Header Pointer (Word)     -   0x270 
[0x00000984]         (0x04)        Next 1st Partition Header (Word)     -   0x460 
[0x00000988]         (0x08)        Partition Count (Wrong Info)         -   0x00 
[0x0000098c]         (0x0C)        Image Length (Wrong Info)            -   0x01 
[0x00000998]         (0x10)        Image Name                           -   bl31.elf




~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
::::::::::::::::::::::::::::::::::::::::   I M A G E    H E A D E R  :::::::::::::::::::::::::::::::::::::::::
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~    Image 4    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
<Flash Address>     <Offset>            <Description>                     <Interpretation>

[0x000009c0]         (0x00)        Next Image Header Pointer (Word)     -   0x00 
[0x000009c4]         (0x04)        Next 1st Partition Header (Word)     -   0x470 
[0x000009c8]         (0x08)        Partition Count (Wrong Info)         -   0x00 
[0x000009cc]         (0x0C)        Image Length (Wrong Info)            -   0x01 
[0x000009dc]         (0x10)        Image Name                           -   u-boot.elf




~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
::::::::::::::::::::::::::::::  P A R T I T I O N    H E A D E R    T A B L E   ::::::::::::::::::::::::::::::
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~    Partition 1    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
<Flash Address>     <Offset>            <Description>                     <Interpretation>

[0x00001100]         (0x00)        Encrypted Data Length (Word)         -   0xdb6e 
[0x00001104]         (0x04)        Unencrypted Data Length (Word)       -   0xdb6e 
[0x00001108]         (0x08)        Total Data Length (Word)             -   0xdb6e 
[0x0000110c]         (0x0c)        Next Partition Header Offset (Word)  -   0x450 
[0x00001110]         (0x10)        Destination Execution Address LO     -   0xfffc0000 
[0x00001114]         (0x14)        Destination Execution Address HI     -   0x00 
[0x00001118]         (0x18)        Destination Load Address LO          -   0xfffc0000 
[0x0000111c]         (0x1c)        Destination Load Address HI          -   0x00 s
[0x00001120]         (0x20)        Actual Partition Offset (Word)       -   0xa00 
[0x00001124]         (0x24)        Attributes                           -   0x116 
                                                                          # Endianness         : [Little Endian]
                                                                          # Partition Owner    : [FSBL]
                                                                          # Authentication     : [None]
                                                                          # Checksum Type      : [None]
                                                                          # Destination CPU    : [A53-0]
                                                                          # Encryption Present : [Unencrypted]
                                                                          # Destination Device : [PS]
                                                                          # A53 Exec State     : [AARCH-64]
                                                                          # Exception Level    : [EL-3]
                                                                          # Trust Zone         : [Non-Secure]
[0x00001128]         (0x28)        Section Count                        -   0x01 
[0x0000112c]         (0x2c)        Checksum Offset (Word)               -   0x00 
[0x00001130]         (0x30)        Corresponding Image Header Off (Word)-   0x240 
[0x00001134]         (0x34)        Auth. Cert. Offset (Word)            -   0x00 
[0x0000113c]         (0x3c)        Partition Header Checksum            -   0x55c0e 





~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
::::::::::::::::::::::::::::::  P A R T I T I O N    H E A D E R    T A B L E   ::::::::::::::::::::::::::::::
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~    Partition 2    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
<Flash Address>     <Offset>            <Description>                     <Interpretation>

[0x00001140]         (0x00)        Encrypted Data Length (Word)         -   0x153e27 
[0x00001144]         (0x04)        Unencrypted Data Length (Word)       -   0x153e27 
[0x00001148]         (0x08)        Total Data Length (Word)             -   0x153e27 
[0x0000114c]         (0x0c)        Next Partition Header Offset (Word)  -   0x460 
[0x00001150]         (0x10)        Destination Execution Address LO     -   0x00 
[0x00001154]         (0x14)        Destination Execution Address HI     -   0x00 
[0x00001158]         (0x18)        Destination Load Address LO          -   0xffffffff 
[0x0000115c]         (0x1c)        Destination Load Address HI          -   0x00 
[0x00001160]         (0x20)        Actual Partition Offset (Word)       -   0xe570 
[0x00001164]         (0x24)        Attributes                           -   0x26 
                                                                          # Endianness         : [Little Endian]
                                                                          # Partition Owner    : [FSBL]
                                                                          # Authentication     : [None]
                                                                          # Checksum Type      : [None]
                                                                          # Destination CPU    : [None]
                                                                          # Encryption Present : [Unencrypted]
                                                                          # Destination Device : [PL]
                                                                          # A53 Exec State     : [AARCH-64]
                                                                          # Exception Level    : [EL-3]
                                                                          # Trust Zone         : [Non-Secure]
[0x00001168]         (0x28)        Section Count                        -   0x01 
[0x0000116c]         (0x2c)        Checksum Offset (Word)               -   0x00 
[0x00001170]         (0x30)        Corresponding Image Header Off (Word)-   0x250 
[0x00001174]         (0x34)        Auth. Cert. Offset (Word)            -   0x00 
[0x0000117c]         (0x3c)        Partition Header Checksum            -   0xffbf5943 





~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
::::::::::::::::::::::::::::::  P A R T I T I O N    H E A D E R    T A B L E   ::::::::::::::::::::::::::::::
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~    Partition 3    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
<Flash Address>     <Offset>            <Description>                     <Interpretation>

[0x00001180]         (0x00)        Encrypted Data Length (Word)         -   0x31f4 
[0x00001184]         (0x04)        Unencrypted Data Length (Word)       -   0x31f4 
[0x00001188]         (0x08)        Total Data Length (Word)             -   0x31f4 
[0x0000118c]         (0x0c)        Next Partition Header Offset (Word)  -   0x470 
[0x00001190]         (0x10)        Destination Execution Address LO     -   0xfffea000 
[0x00001194]         (0x14)        Destination Execution Address HI     -   0x00 
[0x00001198]         (0x18)        Destination Load Address LO          -   0xfffea000 
[0x0000119c]         (0x1c)        Destination Load Address HI          -   0x00 
[0x000011a0]         (0x20)        Actual Partition Offset (Word)       -   0x1623a0 
[0x000011a4]         (0x24)        Attributes                           -   0x117 
                                                                          # Endianness         : [Little Endian]
                                                                          # Partition Owner    : [FSBL]
                                                                          # Authentication     : [None]
                                                                          # Checksum Type      : [None]
                                                                          # Destination CPU    : [A53-0]
                                                                          # Encryption Present : [Unencrypted]
                                                                          # Destination Device : [PS]
                                                                          # A53 Exec State     : [AARCH-64]
                                                                          # Exception Level    : [EL-3]
                                                                          # Trust Zone         : [Secure]
[0x000011a8]         (0x28)        Section Count                        -   0x01 
[0x000011ac]         (0x2c)        Checksum Offset (Word)               -   0x00 
[0x000011b0]         (0x30)        Corresponding Image Header Off (Word)-   0x260 
[0x000011b4]         (0x34)        Auth. Cert. Offset (Word)            -   0x00 
[0x000011bc]         (0x3c)        Partition Header Checksum            -   0xffebfe99 





~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
::::::::::::::::::::::::::::::  P A R T I T I O N    H E A D E R    T A B L E   ::::::::::::::::::::::::::::::
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~    Partition 4    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
<Flash Address>     <Offset>            <Description>                     <Interpretation>

[0x000011c0]         (0x00)        Encrypted Data Length (Word)         -   0x237aa 
[0x000011c4]         (0x04)        Unencrypted Data Length (Word)       -   0x237aa 
[0x000011c8]         (0x08)        Total Data Length (Word)             -   0x237aa 
[0x000011cc]         (0x0c)        Next Partition Header Offset (Word)  -   0x00 
[0x000011d0]         (0x10)        Destination Execution Address LO     -   0x8000000 
[0x000011d4]         (0x14)        Destination Execution Address HI     -   0x00 
[0x000011d8]         (0x18)        Destination Load Address LO          -   0x8000000 
[0x000011dc]         (0x1c)        Destination Load Address HI          -   0x00 
[0x000011e0]         (0x20)        Actual Partition Offset (Word)       -   0x1655a0 
[0x000011e4]         (0x24)        Attributes                           -   0x114 
                                                                          # Endianness         : [Little Endian]
                                                                          # Partition Owner    : [FSBL]
                                                                          # Authentication     : [None]
                                                                          # Checksum Type      : [None]
                                                                          # Destination CPU    : [A53-0]
                                                                          # Encryption Present : [Unencrypted]
                                                                          # Destination Device : [PS]
                                                                          # A53 Exec State     : [AARCH-64]
                                                                          # Exception Level    : [EL-2]
                                                                          # Trust Zone         : [Non-Secure]
[0x000011e8]         (0x28)        Section Count                        -   0x01 
[0x000011ec]         (0x2c)        Checksum Offset (Word)               -   0x00 
[0x000011f0]         (0x30)        Corresponding Image Header Off (Word)-   0x270 
[0x000011f4]         (0x34)        Auth. Cert. Offset (Word)            -   0x00 
[0x000011fc]         (0x3c)        Partition Header Checksum            -   0xefe2ffd9 





~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
::::::::::::::::::::::::::::::::::::::  P A R T I T I O N    D A T A   :::::::::::::::::::::::::::::::::::::::
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~    Partition 1    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

[0x00002800]
	Refer file "_partition_1.txt"
[0x0001036e]




~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
::::::::::::::::::::::::::::::::::::::  P A R T I T I O N    D A T A   :::::::::::::::::::::::::::::::::::::::
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~    Partition 2    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

[0x000395c0]
	Refer file "_partition_2.txt"
[0x0018d3e7]




~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
::::::::::::::::::::::::::::::::::::::  P A R T I T I O N    D A T A   :::::::::::::::::::::::::::::::::::::::
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~    Partition 3    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

[0x00588e80]
	Refer file "_partition_3.txt"
[0x0058c074]




~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
::::::::::::::::::::::::::::::::::::::  P A R T I T I O N    D A T A   :::::::::::::::::::::::::::::::::::::::
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~    Partition 4    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

[0x00595680]
	Refer file "_partition_4.txt"

```


From this output we can see the four images that were use to make up this bootfile.

1. `zynqmp_fsbl.elf` -- This is the First State Bootloader (FSBL)
2. `download.bit` -- Implementation bitstream
3. `bl31.elf` -- Arm Trusted Firmware
4. `u-boot.elf` -- U-Boot

What is not inclued in here is the device tree file, the Linux kernel, or a ramdisk.  While these files are referred to in much of the documentation for the bootfile they are now instead packaged up in a Flattened Image Tree (FIT) file that is used by u-boot.

## FIT

The FIT file mentioned above is normally named  `image.ub` and included in u-boot tools is `dumpimage` for analyzing the content.

```bash
[bashton@xilinx images]$ dumpimage -l image.ub 
FIT description: U-Boot fitImage for PetaLinux/4.14-xilinx-v2018.2+gitAUTOINC+ad4cd988ba/ultra96-zynqmp
Created:         Fri Jun 15 10:04:24 2018
 Image 0 (kernel@1)
  Description:  Linux kernel
  Created:      Fri Jun 15 10:04:24 2018
  Type:         Kernel Image
  Compression:  gzip compressed
  Data Size:    6960372 Bytes = 6797.24 KiB = 6.64 MiB
  Architecture: AArch64
  OS:           Linux
  Load Address: 0x00080000
  Entry Point:  0x00080000
  Hash algo:    sha1
  Hash value:   b978baa68df14b8ef4a1e659f45d6138a3fbef3b
 Image 1 (fdt@system-top.dtb)
  Description:  Flattened Device Tree blob
  Created:      Fri Jun 15 10:04:24 2018
  Type:         Flat Device Tree
  Compression:  uncompressed
  Data Size:    37182 Bytes = 36.31 KiB = 0.04 MiB
  Architecture: AArch64
  Hash algo:    sha1
  Hash value:   97d696a2f22bfe8b68f778b0e9568d273972bda6
 Default Configuration: 'conf@system-top.dtb'
 Configuration 0 (conf@system-top.dtb)
  Description:  1 Linux kernel, FDT blob
  Kernel:       kernel@1
  FDT:          fdt@system-top.dtb
```


# Generating a Minimal BOOT.bin

For this we are going to try and build all the bits to get us to a u-boot prompt

The bif file should look something like this
```
the_ROM_image:
{
     [pmufw_image] pmufw.elf
     [bootloader,destination_cpu=a53-0] zynqmp_fsbl.elf
     download.bit
     [destination_cpu=a53-0,exception_level=el-3,trustzone] bl31.elf
     [destination_cpu=a53-0,exception_level=el-2] u-boot.elf
}
```
and is used to generate BOOT.bin:
> bootgen -image boot.bif -arch zynqmp -w -o i BOOT.bin
